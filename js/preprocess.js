/* "Optimizes" nodes and edges by dropping uninteresting ones. (for example,
 * nodes with no edges).
 */
/* jshint devel:true */

'use strict';

/* find user nodes and remove invalid edges */
function filterEdges(data, ratelimit_max) {
    // map userID to nodes
    var users = {};
    //data.nodes.length = 10000;
    data.nodes.forEach(function (user, i) {
        users[user.userid] = user;
    });

    var ratelimit_count = 0;
    function ratelimit() {
        return ++ratelimit_count <= ratelimit_max;
    }
    // filter away invalid edges
    data.edges = data.edges.filter(function (link, i) {
        var invalid = false;
        if (!(link.source in users)) {
            if (ratelimit()) console.warn('Dropping invalid source user',
                link.source, 'at line', (i + 1), link);
            invalid = true;
        }
        if (!(link.target in users)) {
            if (ratelimit()) console.warn('Dropping invalid target user',
                link.target, 'at line', (i + 1), link);
            invalid = true;
        }
        if (link.source === link.target) {
            if (ratelimit()) console.warn('Dropping self-referencing user',
                link.target, 'at line', (i + 1), link);
            invalid = true;
        }
        return !invalid;
    });
    if (ratelimit_max > 0 && ratelimit_count > ratelimit_max) {
        console.log('Supressed', ratelimit_count, 'messages');
    }
}

function preprocess(data, options) {
    console.log('Initial nodes count:', data.nodes.length);
    console.log('Initial edges count:', data.edges.length);
    filterEdges(data, 10);
    console.log('Valid edges count:', data.edges.length);

    if (options.minTweetCount > 0) {
        /* filter away users with almost no tweets */
        data.nodes = data.nodes.filter(function (node) {
            return node.tweetCount >= options.minTweetCount;
        });
        console.log('Nodes count (ignoring users with fewer than',
            options.minTweetCount, 'tweets):', data.nodes.length);
        filterEdges(data, 0);
    }

    // find all related users by userID
    var hasRelations = {};
    data.edges.forEach(function (link) {
        hasRelations[link.target] = 1;
        hasRelations[link.source] = 1;
    });

    if (options.kill_loners) {
        var hasRelated = {};
        data.nodes = data.nodes.filter(function (d) {
            /* Uncomment if it seems to be useful.
            if (!(d.userid in hasRelations) && d.tweetCount >= 100) {
                console.log('Keeping lonely user with', d.tweetCount, 'tweets');
                return true;
            }
            */
            return d.userid in hasRelations;
        });
        console.log('Nodes count (after dropping loners):', data.nodes.length);
    }

    // link edges with nodes
    var users = {};
    data.nodes.forEach(function (user, i) {
        users[user.userid] = user;
    });

    // change userID of relation edges to indices
    data.edges.map(function (link) {
        // replace source/target user IDs by their respective (user) nodes
        link.source = users[link.source];
        link.target = users[link.target];
        // for faster lookup, store neighboring nodes per node
        link.source.relatedTo.push(link.target.userid);
        link.target.relatedFrom.push(link.source.userid);
    });
}
